# -*- coding: utf-8 -*-
import json
from unittest import TestCase
from unittest.mock import Mock, patch
from app import app
from app.mail_sender import Mailgun, Sendgrid, MailgunException, SendgridException
from requests.exceptions import HTTPError, Timeout
from flask import Response


class MailsenderTestCase(TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.proper_data = {
            'name_from':'Test name',
            'email_from':'test@example.com',
            'subject':'Test subject',
            'html':'<p>Test html</p>',
            'send_to':'test@example.com'
        }

    def tearDown(self):
        pass

    def _raise_send_http_error(self, send_url, msg, **kwargs):
        raise HTTPError

    def _raise_send_timeout_error(self, send_url, msg, **kwargs):
        raise Timeout

    def _return_valid_response(self, send_url, msg, **kwargs):
        return Response(response='ok', status=200, mimetype='application/json')

    def test_open_form(self):
        resp = self.app.get('/')
        assert b'Mail Sender' in resp.data

    def test_missing_fields(self):
        improper_data = self.proper_data.copy()
        improper_data.pop('email_from', None)

        resp = self.app.post('/api/send', data=json.dumps(improper_data),
                            content_type='application/json')

        assert b'email_from' in resp.data
        assert b'Missing' in resp.data

    def test_improper_email(self):
        improper_data = self.proper_data.copy()
        improper_data['email_from'] = 'aaaa'

        resp = self.app.post('/api/send', data=json.dumps(improper_data),
                            content_type='application/json')

        assert b'email_from' in resp.data
        assert b'Not a valid email' in resp.data

    @patch.object(Mailgun, '_send', _raise_send_http_error)
    @patch.object(Sendgrid, '_send', _raise_send_http_error)
    def test_mailgun_sendgrid_error(self):

        resp = self.app.post('/api/send', data=json.dumps(self.proper_data),
                            content_type='application/json')

        assert b'No backends' in resp.data

    @patch.object(Mailgun, '_send', _raise_send_http_error)
    @patch.object(Sendgrid, '_send', _return_valid_response)
    def test_mailgun_error(self):

        resp = self.app.post('/api/send', data=json.dumps(self.proper_data),
                            content_type='application/json')
        assert b'Email sent with sendgrid' in resp.data

    @patch.object(Mailgun, '_send', _return_valid_response)
    def test_mailgun_proper(self):

        resp = self.app.post('/api/send', data=json.dumps(self.proper_data),
                            content_type='application/json')

        assert b'Email sent with mailgun' in resp.data


if __name__ == '__main__':
    unittest.main()
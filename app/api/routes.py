# -*- coding: utf-8 -*-
import requests
import json
from flask import request, Response
from marshmallow import Schema, fields, ValidationError
from requests.exceptions import HTTPError, Timeout
from app import app
from ..mail_sender import Mailgun, Message, Sendgrid

mg = Mailgun(app.config)
sg = Sendgrid(app.config)

#marshmallow schema for parsing request params
class MailSchema(Schema):
    name_from=fields.Str()
    email_from=fields.Email(required=True)
    subject=fields.Str(required=True)
    html=fields.Str()
    send_to=fields.Str(required=True)

mail_schema = MailSchema()

@app.route('/api/send', methods=['POST'])
def send_mail():
    
    if request.method == 'POST':

        #load the schema, check for errors
        data, errors = mail_schema.load(json.loads(request.data))

        #return bad requst if errors present
        if errors:
            return Response(response=json.dumps({'errors': errors}),
                            status=400, mimetype='application/json')

        #create the message data
        msg = Message(**data)

        #try sending wigh mailgun
        try:
            sent_with = 'mailgun'
            response = mg.prepare_and_send(msg)

        except (HTTPError, Timeout) as e:
            #if mailgun  had errors it may not be available, try sendgrid
            try:
                sent_with='sendgrid'
                response = sg.prepare_and_send(msg)

            except (HTTPError, Timeout) as e:
                return Response(
                    response=json.dumps({
                        'errors': "No backends available. Try again later."}),
                    status=500, mimetype='application/json')
                
        #if no errors present return info about sending
        return Response(
                response=json.dumps({
                        'message': 'Email sent with %s' % sent_with}),
                            status=200, mimetype='application/json')
        
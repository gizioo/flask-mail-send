(function () {

  'use strict';

  angular.module('MailSender5000App', ['ui.tinymce'])

  .controller('MailSenderController', ['$scope', '$log', '$http', '$timeout',
    function($scope, $log, $http, $timeout) {
      $scope.tinymceOptions = {
          selector: '#html',
          height: 400,
          statusbar: false,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help'
          ],
          toolbar: 'formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
          content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
      };
      $scope.got_results = false;
      $scope.result_message = null;

    $scope.getResults = function() {

      $log.log("test");

      // get the URL from the input
      var mail_data = {
        'name_from': $scope.name_from,
        'email_from': $scope.email_from,
        'subject': $scope.subject,
        'html':$scope.html,
        'send_to':$scope.send_to
      }
      // fire the API request

      
      $http.post('/api/send', mail_data).
        success(function(results) {
          $scope.got_results=true;
          $scope.result_message = results.message;
          
        }).
        error(function(error) {
          $scope.got_results=true;
          $scope.result_message = results.errors;
        });

    };
    $scope.restart = function() {
        $scope.got_results = false;
        $scope.result_message = null;
        $scope.name_from = null;
        $scope.email_from = null;
        $scope.send_to = null;
        $scope.subject = null;
        $scope.html = null;

    }
  }
  ]);

}());


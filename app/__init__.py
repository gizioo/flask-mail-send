import logging
from flask import Flask, render_template, request, jsonify
from logging.handlers import RotatingFileHandler

app = Flask(__name__)
app.config.from_object('app.config')



from .mail_sender.mailgun import Mailgun
from .mail_sender.message import Message
from .api import *

@app.before_first_request
def setup_logging():
    print("SEtting up")
    app.log = logging.getLogger()

    fh = RotatingFileHandler('log.txt', mode='w')
    app.log.addHandler(fh)

    ch = logging.StreamHandler()
    app.log.addHandler(ch)
    app.log.setLevel(logging.INFO)

@app.before_request
def log_request_info():
    app.log.info('Headers: %s', request.headers)
    app.log.info('Body: %s', request.get_data())

@app.route('/')
def hello():
    return render_template('index.html')

if __name__ == '__main__':
    app.run()

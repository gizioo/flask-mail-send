# -*- coding: utf-8 -*-
import requests
from .message import Message
from .abstract import Mailsender


class MailgunException(Exception):
    pass


class Mailgun(Mailsender):

    #init the class
    def __init__(self, config):
        self.domain = config['MAILGUN_DOMAIN']
        self.api_key = config['MAILGUN_API_KEY'].encode('utf-8')
        self.api_url = config['MAILGUN_API_URL']

        if self.api_key is None:
            raise MailgunException("No mailgun key supplied.")

    #actual method for posting to Mailgun
    def _send(self, send_url, message, credentials=None):
        response = requests.post(send_url,
                                 auth=credentials,
                                 data=message)

        response.raise_for_status()
        return response

    #prepare the message data before and call send method
    def prepare_and_send(self, message):
        message_data = self.prepare_message_data(message)
        send_url = self.send_url
        credentials = self.credentials
        return self._send(send_url, message_data, credentials=credentials)

    #put together the mailgun api rout
    def api_route(self, *parts):
        parts = [self.api_url] + [p for p in parts]
        return '/'.join(s.strip('/') for s in parts)

    #prepare the message data for sending
    def prepare_message_data(self, message):
        message_data = {'from': message.envelope_from,
                       'to': message.send_to,
                       'subject': message.subject,
                       'html': message.html}
        return message_data

    #send url property
    @property
    def send_url(self):
        return self.api_route(self.domain, 'messages')

    #mailgun credentials
    @property
    def credentials(self):
        return ('api', self.api_key)
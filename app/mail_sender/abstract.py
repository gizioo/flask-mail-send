# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from .message import Message

#abstract class for creating other classes for mail providers
class Mailsender(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def _send(self, *args, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def prepare_and_send(self, message):
        raise NotImplementedError

    @abstractmethod
    def prepare_message_data(self, message):
        raise NotImplementedError

    @abstractmethod
    def send_message(self, *args, **kwargs):
        self.send(Message(*args, **kwargs))
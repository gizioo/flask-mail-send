# -*- coding: utf-8 -*-
import requests
import json
from .message import Message
from .abstract import Mailsender


class SendgridException(Exception):
    pass

#sendgrid class
class Sendgrid(Mailsender):

    #init the calss with config
    def __init__(self, config):
        self.api_key = config['SENDGRID_API_KEY']
        self.api_url = config['SENDGRID_API_URL']

        if self.api_key is None:
            raise SendgridException("No mailgun key supplied.")

    #actual sen method
    def _send(self, send_url, message, headers=None):
        response = requests.post(send_url,
                                 headers=headers,
                                 data=json.dumps(message))


        response.raise_for_status()
        return response

    #prepare the message and send
    def prepare_and_send(self, message):
        message_data = self.prepare_message_data(message)
        send_url = self.send_url
        headers = self.headers
        return self._send(send_url, message_data, headers=headers)

    #prepere the message data in sendgrid format
    def prepare_message_data(self, message):
        message_data = {'personalizations':[
                            {'to':[{"email": message.send_to}],
                             'subject': message.subject
                             }],
                        'from':{
                            'email': message.email_from,
                            'name': message.name_from
                        },
                        'content':[{
                            'type': 'text/html',
                            'value': message.html
                        }]
                        }
        return message_data

    #put together the sendgrid route
    def api_route(self, *parts):
        parts = [self.api_url] + [p for p in parts]
        return '/'.join(s.strip('/') for s in parts)

    #headers used by sendgrid for authorization
    @property
    def headers(self):
        return {'Authorization': 'Bearer %s' % self.api_key,
                'Content-Type': 'application/json'}

    #sengrid api url
    @property
    def send_url(self):
        return self.api_route('mail', 'send')
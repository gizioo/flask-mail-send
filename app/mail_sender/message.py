# -*- coding: utf-8 -*-

#message class
class Message(object):

    def __init__(self, subject='',
                 name_from=None,
                 email_from=None,
                 send_to=None,
                 html=None
                 ):

        self.subject = subject
        self.html = html
        self.name_from = name_from
        self.email_from = email_from
        self.send_to = send_to

    #property in a format used by mailgun
    @property
    def envelope_from(self):
        return '%s <%s>' % (self.name_from, self.email_from)
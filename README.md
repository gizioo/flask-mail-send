# README #

A simple Flask-AngularJS app for sending emails with Mailgun or Sendgrid

### What is this? ###

This is a demo application for sending emails with Mailgun or Sendgrid.
A minimal frontend is present.

The app will try to send given email with Mailgun. If mailgun throws an error or times out it will retry with Sendgrid.

If both fail - an appropriate message will be given.

The app exposes a REST api:


param    | value
-------- | ----------
Method   | POST
URL      | /mail/send

Data params:

name       | type     | required
---------- | -------- | ----------
name_from  | string   | 
email_from | email    | yes
subject    | string   | yes
html       | string   | 
send_to    | email    | yes

### Structure, inheritance? ###

Mailgun and Sendgrid classes are based on an abstract Mailsender class which forces implementations of all needed methods.

### Tests? ###

The python code has around ~90% coverage.

Mailgun and Sendgrid APIs are mocked in the tests.

### Scalability? ###

The app uses Mailgun and Sendgrid APIs and no database, therofe you can instantiate it to your heart's content.

### How do I get set up? ###

* Clone the repository
* edit config.py to provide keys for Mailgun and Sendgrid
* Run the following commands in your system or virtualenv
~~~
pip install -r requirements.txt
export FLASK_APP=<path_to_run.py>
flask run
~~~
* Or deploy to heroku (simple Procfile included)
